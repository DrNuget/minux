--[[
	MinuxCC GUI Library
	Copyright under GNU GPL v.2.0
]]


--[[
	TODO:

	sliders
	loading bar
	graph

	multiline text field
	component containers/component nesting
	movable window

	takeEvent(event)
	getValues(component)
]]

local gui = {}

local components = {}
local activefield = nil
local fullscreen = nil

local function drawButton(button)
	local text, x, y = button.text, button.x, button.y
	if button.state then
		term.setBackgroundColor(gui.TX)
		term.setTextColor(gui.BT)
	else
		term.setBackgroundColor(gui.BT)
		term.setTextColor(gui.TX)
	end
	term.setCursorPos(x, y)
	term.write(text)
end

local function drawField(field)
	local x, y = field.x, field.y
	paintutils.drawLine(x,y,x+field.size,y,gui.TF)
	term.setCursorPos(x,y)
	term.write(field.text)
end

local function updateField(key)
	local field = components[activefield]
	drawField(field)
	term.setBackgroundColor(gui.TF)
	term.setCursorPos(field.x,field.y)
	term.setCursorBlink(true)
end

local function genRandID(length)
  local str = ""
  for _=1,length do
    local num = math.random(48,109)
    if num >= 58 then num = num + 7 end
    if num >= 91 then num = num + 6 end
    str = str .. string.char(num)
  end
  return str
end


gui.BG=colors.blue
gui.BT=colors.lime
gui.TF=colors.black
gui.TX=colors.white

function gui.init()
	fullscreen=term.current()
end

function gui.createButton(text, x, y)
	local id = genRandID(5)
	components[id]={
		type = "button",
		x = x,
		y = y,
		size = #(text),
		text = text,
		id = id,
		state = false
	}
	drawButton(components[id])
	return id
end

function gui.createToggler(on, off, x, y)
	--on, off = text
	local id = genRandID(5)
	components[id]={
		type = "toggle",
		on = on,
		off = off,
		x = x,
		y = y,
		size = (#on>#off) and #on or #off,
		text = off,
		state = false,
		id = id
	}
	drawButton(components[id])
	return id
end

function gui.createTextField(x, y, length, secret)
	local id = genRandID(5)
	components[id]={
		type = "field",
		x = x,
		y = y,
		size = length,
		text = "",
		id = id,
		secret = secret
	}
	drawField(components[id])
	return id
end

function gui.print(text)
	local x, y = text.x, text.y
	local text, color = text.text, text.color
	term.setCursorPos(x,y)
	term.setBackgroundColor(gui.BG)
	if (color~=nil) then
		term.setTextColor(color)
	else
		term.setTextColor(gui.TX)
	end
	term.write(text)
end

function gui.printConstant(text, x, y, color)
	local id = genRandID(5)
	components[id]={
		type = "text",
		x = x,
		y = y,
		text = text,
		color = color,
		id = id
	}
	gui.print(components[id])
end

function gui.refresh()
	term.setBackgroundColor(gui.BG)
	term.clear()
	if (next(components)) then
		for _,i in pairs(components) do
			if (i.type=="button") then
				drawButton(i)
			elseif (i.type=="toggle") then
				drawButton(i)
			elseif (i.type=="field") then
				drawField(i)
			elseif (i.type=="text") then
				gui.print(i)
			end
		end
	end
end

--[[
	event types:
	button
	buttonUp
	fieldInput

	return values:
	eventType, componentID, state
]]

function gui.listenEvent()
	--return values:
	--eventType, componentID, state
	local listening = true
	while (listening) do
		local event, button, x, y = os.pullEvent()
		if (event=="mouse_click") then
			activefield=nil
			if next(components) then
				for _,i in pairs(components) do
					if (i.type=="button") then
						if (x<=i.x+i.size and x>=i.x and y==i.y) then
							components[i.id].state=true
							drawButton(i)
							return "button", i.id, true
						end
					elseif (i.type=="toggle") then
						if (x<=i.x+i.size and x>=i.x and y==i.y) then
							i.state=not i.state
							i.text = i.state and i.on or i.off
							drawButton(i)
							return "button", i.id, i.state
						end
					elseif (i.type=="field") then
						if (x<=i.x+i.size and x>=i.x and y==i.y) then
							activefield=i.id
							updateField()
							listening=false
						end
					end
				end
			end
		elseif (event=="mouse_up") then
			if next(components) then
				for _,i in pairs(components) do
					if (i.type=="button") then
						if (x<=i.x+i.size and x>=i.x and y==i.y) then
							components[i.id].state=false
							drawButton(i)
							return "buttonUp", i.id, false
						end
					end
				end
			end
		end
	end
	if (activefield~=nil) then
		local field = components[activefield]
		local x, y, length = field.x, field.y, field.size
		local win = window.create(term.current(), x,y,length,1)
		term.redirect(win)
		local input
		if (field.size) then
			input = read("*",nil,nil,field.text)
		else
			input = read(nil,nil,nil,field.text)
		end
		term.redirect(fullscreen)
		field.text=input
		return "fieldInput", activefield, input
	end
end

return gui
