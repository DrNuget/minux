--[[
	MinuxCC Kernel
	This is the integral part of MinuxCC, everything else can be removed at will
]]

-- Basic functions

function split(string, delim)
	local t={}
	if (delim==nil) then
		delim="%s"
	end
	for i in string.gmatch(string, "([^"..delim.."]+)") do
		table.insert(t,i)
	end
	return t
end

-- Variable pool

vpool = {}
vpool[0]=true
vpool[999]=""

-- Script lexer/parser

-- (.*\{.*\})(\{.*\})(.*)


-- ({.*})(.*)
