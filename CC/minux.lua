--[[
	MinuxCC/OC standard library
	Not a mandatory library for any program but recommended to use for applications that follow Minux compliant code standards
	Is included by default if you run a program through the Minux Operating System
]]

local minux = {}

function minux.is_in(table, value)
	for _,i in pairs(table) do
		if i==value then
			return true
		end
	end
	return false
end

function minux.split(string, delim)
	local t={}
	delim==nil and delim="%s"
	for i in string.gmatch(string, "([^"..delim.."]+)") do
		table.insert(t, i)
	end
	return t
end

return minux
